from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.graphics import Color

class TextColorApp(App):

    def build(self):
        # US-1 - Créer la structure du projet avec l'import de kivy et, la classe App() et un premier GridLayout.
        layout = GridLayout(cols=1)

        # US-2: Créer le champ textinput et l'afficher à l'écran.
        self.text_input = TextInput(text='', multiline=False)
        layout.add_widget(self.text_input)

        # US-3: Créer le label et l'afficher à l'écran. Par défaut la valeur du texte dans le label vaut “Entrez du texte”.
        self.label = Label(text='Entrez du texte', font_size='20sp')
        layout.add_widget(self.label)

        # US-4: Créer les boutons Rouge, Bleu, Vert, Jaune, Rose, Marron et les afficher à l'écran.
        color_buttons = [
            ("Rouge", (1, 0, 0, 1)),
            ("Bleu", (0, 0, 1, 1)),
            ("Vert", (0, 1, 0, 1)),
            ("Jaune", (1, 1, 0, 1)),
            ("Rose", (1, 0.5, 0.5, 1)),
            ("Marron", (0.6, 0.4, 0.2, 1))
        ]

        for color_name, color in color_buttons:
            button = Button(text=color_name, on_press=self.change_label_color)
            button.background_color = color
            layout.add_widget(button)

        # US-5: Faire le bind entre le widget saisie et le widget label.
        self.text_input.bind(text=self.update_label_text)

        # US-6: Faire le bind entre les boutons de couleur et le widget label.
        self.color_buttons = [btn for btn in layout.children if isinstance(btn, Button)]
        for button in self.color_buttons:
            button.bind(on_press=self.change_label_color)

        return layout

    def update_label_text(self, instance, value):
        # Update le label lorsque le textInput change
        self.label.text = value

    def change_label_color(self, instance):
        # Change la couleur du label selon la couleurs sélectionnée.
        if instance in self.color_buttons:
            self.label.color = instance.background_color

if __name__ == '__main__':
    TextColorApp().run()
